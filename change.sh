#!/bin/bash

# Check if the file exists.
if [ ! -f "$1" ]; then
  echo "File does not exist."
  exit 1
fi

# Check if the string {{IMAGE_TAG}} is present in the file.
string='{{IMAGE_TAG}}'
if grep -q "$string" "$1"; then
  echo "The string '$string' exists"
  sed -i "s#{{IMAGE_TAG}}#${COMMIT_SHA}#g" "$1"
else
  line=$(grep -n "vkunal/demos" "$1" | cut -d: -f1)
  output=$(sed -n "$line p" "$1")
  IMAGE_TAG=$output
  NEW_IMAGE_TAG=$(echo "$IMAGE_TAG" | awk -F '-' '{ split($NF, arr, ":"); print arr[1] }')
  sed -i "s#$NEW_IMAGE_TAG#${COMMIT_SHA}#g" "$1"
fi

# Check if the string has 